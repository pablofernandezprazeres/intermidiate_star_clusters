/**
 * Created by: Pablo Fernandez Prazeres
 * Date: 18/Jul/2022
 * Time: 19h:29m
 * Description: Code qui prends les donnes d'etoiles et donne un graphique
 */
//=============================================================================
package org.pfp.logger;
//=============================================================================
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//=============================================================================
//=============================================================================
public class Logback {
    //-------------------------------------------------------------------------

    public static final Logger LOGGER = LoggerFactory.getLogger(Logback.class);
    //-------------------------------------------------------------------------
    //It creates a log for every action of the program
    public void init() {

        LOGGER.info("Starting plotting program!");
    }
    //-------------------------------------------------------------------------
    public void close() {
        LOGGER.info("Closing plotting program!");
    }
    //-------------------------------------------------------------------------
}
//=============================================================================
//End of file Logback.java
//=============================================================================
