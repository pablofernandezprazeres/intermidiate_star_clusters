/**
 * Created by: Pablo Fernandez Prazeres
 * Date: 18/Jul/2022
 * Time: 19h:29m
 * Description: Will read the data and create a plot
 */
//=============================================================================
package org.pfp.plot;
//=============================================================================
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import org.apache.commons.csv.CSVRecord;
import org.jfree.data.xy.XYSeries;
import org.pfp.logger.Logback;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
//=============================================================================
//=============================================================================
public class Plot {
    //-------------------------------------------------------------------------
    // This is where all the organised data is
    private List<Double> M_vCol = new ArrayList<Double>();
    private List<Double> BvCol = new ArrayList<Double>();
    private List<Double> RtCol = new ArrayList<Double>();
    //-------------------------------------------------------------------------
    public void parseCsv(String csvFileName)  {
        //The program will open and read the data
        Logback.LOGGER.info("Parsing file:"+ csvFileName);
        try {
            Reader reader = Files.newBufferedReader(Paths.get(csvFileName));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
            readColumns(csvParser);
        }
        catch (IOException e) {
            Logback.LOGGER.error("Error plotting file:"+ csvFileName);
            throw new RuntimeException(e);
        }

        Logback.LOGGER.info("Parsed file:"+ csvFileName);
    }
    //-------------------------------------------------------------------------
    public void readColumns(CSVParser csvParser) throws IOException {
    /* He converts multiple spaces to only one to be able to seperate
       all the data and to organise it into the list above */

        List<CSVRecord> csvRecords = csvParser.getRecords();
        int rowCount = csvRecords.size();

        Logback.LOGGER.info("Parsing row count:"+ rowCount);

        for(int i=0; i < rowCount; i++){
            if(0<i){//skip over header
                CSVRecord csvRecord = csvRecords.get(i);

                String line = csvRecord.get(0).trim().replaceAll(" +", " ");
                String[] splitSeq = line.split(" ");

                double M_v = Double.parseDouble(splitSeq[7]);
                M_vCol.add(M_v);

                double Bv = Double.parseDouble(splitSeq[3]);
                BvCol.add(Bv);

                double Rt = Double.parseDouble(splitSeq[13]);
                RtCol.add(Rt);

            }
        }
        Logback.LOGGER.info("Row count parsed ");

    }
    //-------------------------------------------------------------------------
  public List<Double> F3() {
        //It calculates the theoretical result
      List<Double> result = new ArrayList<Double>();
      int rowCount = BvCol.size();
      for(int i=0; i < rowCount; i++){
          double Bv = BvCol.get(i);
          double r = Math.pow((Bv),5)*(31.725) -
                     Math.pow( (Bv),4)*142.328 +
                     Math.pow((Bv),3)*251.301 -
                     Math.pow((Bv),2)*219.7623 +
                     (Bv)*100.782-
                     15.262;
          Logback.LOGGER.info("Calculating Th rowCount:"+ i);
          result.add(r);
      }
      Logback.LOGGER.info("Finished calculating");
      return result;
  }
    //-------------------------------------------------------------------------
  public void showPlot() {
        //It merges the data that we want into dataSeries then puts it in the frame and shows it
      int rowCount = BvCol.size();

      XYSeries dataSeries_1 = new XYSeries("Bv-M_v");
      for(int i=0; i < rowCount; i++){
          dataSeries_1.add(BvCol.get(i),M_vCol.get(i));    //add to data series values (x,y)
          Logback.LOGGER.info("Creating dataSeries_1 row:"+ i);
      }

      XYSeries dataSeries_2 = new XYSeries("Bv-theoretical");
      List<Double> f3 = F3();
      for(int i=0; i < rowCount; i++){
          dataSeries_2.add(BvCol.get(i), f3.get(i));    //add to data series values (x,y)
          Logback.LOGGER.info("Creating dataSeries_2 row:"+ i);
      }
      Logback.LOGGER.info("Creating plot");
      Frame frame = new Frame("El Master Hacker"
              , "Super_Graphica"
              , "B-V"
              , "Mv"
              , dataSeries_1
              , dataSeries_2);
      frame.pack();
      frame.setVisible(true);
      Logback.LOGGER.info("Plot created");
  }
    //-------------------------------------------------------------------------
}
//=============================================================================
//End of Plot.java
//=============================================================================
