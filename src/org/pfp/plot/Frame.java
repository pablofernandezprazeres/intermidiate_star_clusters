package org.pfp.plot;
//=============================================================================
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.pfp.logger.Logback;

import javax.swing.*;
//-----------------------------------------------------------------------------
public class Frame extends JFrame {
//-----------------------------------------------------------------------------
    private static final long serialVersionUID = 1L;
    //-------------------------------------------------------------------------
    public Frame(String applicationTitle
                , String chartTitle
                , String xAxisLabel
                , String yAxisLabel
                , XYSeries dataSeries_1
                , XYSeries dataSeries_2){
        //It creates the frame for the plot

        super(applicationTitle);

        Logback.LOGGER.info("Creating frame");

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(dataSeries_1);
        dataset.addSeries(dataSeries_2);

        // based on the dataset we create the chart
        JFreeChart pieChart = ChartFactory.createScatterPlot(chartTitle
                , xAxisLabel
                , yAxisLabel
                , dataset
                , PlotOrientation.VERTICAL
                , true
                , true
                , false);

        // Adding chart into a chart panel
        ChartPanel chartPanel = new ChartPanel(pieChart);

        // settind default size
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));

        // add to contentPane
        setContentPane(chartPanel);
        Logback.LOGGER.info("Frame built");
    }
    //-------------------------------------------------------------------------
}
//=============================================================================
//End of Frame.java
//=============================================================================
