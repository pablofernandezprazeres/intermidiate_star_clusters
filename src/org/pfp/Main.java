 /**
 * Created by: Pablo Fernandez Prazeres
 * Date: 13/Jul/2022
 * Time: 19h:49m
 * Description: Code qui prends les donnes d'etoiles et donne un graphique
 */
//=============================================================================
 package org.pfp;
//=============================================================================
import org.pfp.logger.Logback;
import org.pfp.plot.Plot;

 //=============================================================================
//=============================================================================
public class Main {
    //-------------------------------------------------------------------------
    public static void main(String[] args) {
        //It starts the log start parseCsv and shows the plot

        Logback logback = new Logback();

        logback.init();
        Plot plot = new Plot();

        plot.parseCsv("/home/pablo/project/intermidiate_star_clusters/in/table_Pleiades.tex");
        plot.showPlot();

        logback.close();
    }
    //-------------------------------------------------------------------------
}
//=============================================================================
//End of file org.pfp.Main.java
//=============================================================================
